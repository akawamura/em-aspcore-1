﻿namespace EmAspCore.Entity
{
    public class Employee
    {
        public int employeeId { get; set; }
        public string employeeName { get; set; }
        public string employeeKana { get; set; }
        public string mailAddress { get; set; }
        public string password { get; set; }
        public int departmentId { get; set; }

    }
}
