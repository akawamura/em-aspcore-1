﻿using EmAspCore.Models;
using EmAspCore.Services;
using Microsoft.AspNetCore.Mvc;

namespace EmAspCore.Controllers
{
    public class EmployeeController : Controller
    {
        public IActionResult Index()
        {
            EmployeeViewModel model = new EmployeeViewModel();

            // 初期表示用に検索をしておく
            SearchService service = new SearchService();

            // Modelに詰める
            model.employees = service.SelectAllEmployee();
            model.departments = service.SelectAllDepartment();

            return View(model);
        }
    }
}