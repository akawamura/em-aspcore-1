﻿using EmAspCore.Entity;
using System.Collections.Generic;

namespace EmAspCore.Models
{
    public class EmployeeViewModel
    {
        // 検索結果の格納
        public List<Employee> employees { get; set; }
        public List<Department> departments { get; set; }
    }
}
